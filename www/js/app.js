// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ionic-material', 'ionMdInput', 'starter.filters','ngCordova'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {

    // Turn off caching for demo simplicity's sake
    $ionicConfigProvider.views.maxCache(0);

    /*
    // Turn off back button text
    $ionicConfigProvider.backButton.previousTitleText(false);
    */
    // ConnectivityMonitor.startWatching();
    $httpProvider.interceptors.push('interceptorAllRequest');

    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })

    .state('app.approved', {
        url: '/approved',
        views: {
            'menuContent': {
                templateUrl: 'templates/approved.html',
                controller: 'ApprovedCtrl'
            }
        }
    })

    .state('app.drivers', {
        url: '/drivers',
        views: {
            'menuContent': {
                templateUrl: 'templates/drivers.html',
                controller: 'DriversCtrl'
            }
        }
    })

    .state('app.inbox', {
        url: '/inbox',
        views: {
            'menuContent': {
                templateUrl: 'templates/inbox.html',
                controller: 'InboxCtrl'
            }
        }
    })

    .state('app.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            }
        }
    })

    .state('app.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl'
            }
        }
    })
    
    .state('app.detail', {
        url: '/details/:type/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/detail.html',
                controller: 'DetailCtrl'
            },
            /*'fabContent': {
                template: '<button id="fab-approve" style="pointer-events: auto;" ng-click="fabMenuToggle()" class="button button-fab button-fab-top-right expanded button-energized-900 flap"><i class="icon ion-android-menu"></i></button>1',
                controller: function ($timeout) {
                    $timeout(function () {
                        try {
                            document.getElementById('fab-approve').classList.toggle('on');
                            // document.getElementById('overlay1').classList.remove('active');
                        } catch (e) {
                            // console.log(e);
                        }
                    }, 200);
                }
            }*/
        }
    })
    
    .state('app.contact', {
        url: '/contact/',
        views: {
            'menuContent': {
                templateUrl: 'templates/contact.html',
                controller: 'ContactCtrl'
            },
        }
    })
    
    .state('app.password', {
        url: '/password/',
        views: {
            'menuContent': {
                templateUrl: 'templates/password.html',
                controller: 'PasswordCtrl'
            },
        }
    })
    
    .state('app.about', {
        url: '/about/',
        views: {
            'menuContent': {
                templateUrl: 'templates/about.html',
                controller: 'AboutCtrl'
            },
        }
    })
    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');
});
