angular.module('starter.services', [])
        .factory('Camera', ['$q', function($q) {
                return {
                getPicture: function(options) {
                    var q = $q.defer();
        
                    navigator.camera.getPicture(function(result) {
                    // Do any magic you need
                    q.resolve(result);
                    }, function(err) {
                    q.reject(err);
                    }, options);
        
                    return q.promise;
                }
                }
            }])
        .factory('ClockSrv', function($interval){
                var clock = null;
                var service = {
                    startClock: function(fn, time){
                        if(clock === null){
                            clock = $interval(fn, time);
                        }
                    },
                    stopClock: function(){
                        if(clock !== null){
                            $interval.cancel(clock);
                            clock = null;
                        }
                    }
                };
              
                return service;
            })
        .factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork){

            return {
              isOnline: function(){
                if(ionic.Platform.isWebView()){
                  return $cordovaNetwork.isOnline();
                } else {
                  return navigator.onLine;
                }
              },
              isOffline: function(){
                if(ionic.Platform.isWebView()){
                  return !$cordovaNetwork.isOnline();
                } else {
                  return !navigator.onLine;
                }
              },
              startWatching: function(){
                if(ionic.Platform.isWebView()){

                  $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                    console.log("went online");
                  });

                  $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                    console.log("went offline");
                  });

                } else {

                  window.addEventListener("online", function(e) {
                    console.log("went online");
                  }, false);

                  window.addEventListener("offline", function(e) {
                    console.log("went offline");
                  }, false);
                }
              }
            }
        })
        .factory('interceptorAllRequest', ['ConnectivityMonitor','$injector',function(ConnectivityMonitor,$injector) {
          return {
            request: function(config) {
              if(ConnectivityMonitor.isOffline()){
                var ionicPopup = $injector.get('$ionicPopup');
                var popUp = ionicPopup.alert({
                  title: language['err'] ,
                  template: language['tdkKonek'],
                  okType: 'button-assertive',
                  enableBackdropDismiss: true
                });
                popUp.then(function(res) {
                  console.log('something');
                });
              }
              return config;
            }
          };
        }])
        .factory('tmsService', function ($http, $window) {
        //    var baseUrl = "http://timesheet.mpm-rent.net/tsm_live/index.php/services_approve/";
           var baseUrl = "http://182.23.44.169/tsm_web/index.php/services_approve/";
            return {
                doLogin: function (data) {
                    return $http.post(baseUrl + 'doLogin', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getDriver: function () {
                    return $http.post(baseUrl + 'get_driver', null, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                checkUpdate: function (version) {
                    return $http.post(baseUrl + 'check_update/' + version, null, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                checkCountry: function (data) {
                    return $http.get(baseUrl + 'get_location', data, {
                        headers: {
                            'Access-Control-Allow-Origin' : '*',
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                addLocation: function () {
                    return $http.post(baseUrl + 'addLocation', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getApproveById: function (data) {
                    return $http.post(baseUrl + 'approve_timesheet', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getNotApproveById: function (data) {
                    return $http.post(baseUrl + 'not_approve_timesheet', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getAllPending: function () {
                    return $http.get(baseUrl + 'get_timesheets/pending/' + $window.localStorage['username_pic']);
                },
                getAllPendingNotif: function () {
                    return $http.get(baseUrl + 'get_timesheets_notif/' + $window.localStorage['username_pic']);
                },
                getAllApprove: function () {
                    return $http.get(baseUrl + 'get_timesheets/approved/' + $window.localStorage['username_pic']);
                },
                getAllDrivers: function (data) {
                    return $http.post(baseUrl + 'get_driver', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getDriver: function (data) {
                    return $http.get(baseUrl + 'get_driver/' + id);
                },
                getTimesheetById: function ($data) {
                    return $http.get(baseUrl + 'get_timesheets_by_id/' + $data);
                },
                getChangePassword: function (data) {
                    return $http.post(baseUrl + 'changePassword', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                searchTimesheet: function (data) {
                    return $http.post(baseUrl + 'lists_period', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getMessage: function (data) {
                    return $http.get(baseUrl + 'getMessage/' + data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getUnreadMessage: function (data) {
                    return $http.get(baseUrl + 'getUnreadMessage/' + data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getAllMessage: function (data) {
                    return $http.get(baseUrl + 'getAllMessage/' + data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getMessageDetail: function (data) {
                    return $http.get(baseUrl + 'getMessageDetail/' + data.user + '/' + data.id, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getLanguage: function () {
                    return $http.get(baseUrl + 'getLanguage/' , {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getNotifAgree: function (data) {
                    return $http.post(baseUrl + 'getNotifAgree', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getAgree: function (data) {
                    return $http.post(baseUrl + 'agree', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
            };
        });
