/* global angular, document, window */
'use strict';
// var server = "http://timesheet.mpm-rent.net/";
// var server = "http://192.168.22.113:8100/";
var tmsService = angular.module('tmsService',['ionic']);
var user_pic = "-";
var ind = {}; 
var eng = {}; 
var jap = {};
var language = null;

function getFormattedDate(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return year + '-' + month + '-' + day;
}

function getFormattedTime(date) {
    hour = date.getHours();
    minute = date.getMinutes();
    second = date.getSeconds();
    return hour + ':' + minute + ':' + second;
}

function getInitial(text) {
    var initials = text.match(/\b\w/g) || [];
    initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
    return initials;
}

function radioChecked(val) {
    for (var i = 1; i <= 5; i++) {
        document.getElementById('radioImg'+i).src='img/Bintang-Abu.png';
    }
    for (var i = 1; i <= val; i++) {
        document.getElementById('radioImg'+i).src='img/Bintang-kuning.png';
    }
}

function onChangeReason(val) {
  if(val==0){
    document.getElementById('reason').style.display ="none";
  } else {
    document.getElementById('reason').style.display ="block";
    document.getElementById('reason_txt').value = "";
  }
}


function alertButton() {
    var cek = 1;
    var counter = 0;
    var i = setInterval(function(){
        // do your thing
        if (cek) {
            try {
                document.getElementById('backAgree').style.backgroundColor='#387ef5';
                document.getElementById('textAgree').style.color='white';
            } catch (e){
                console.log(e.message);
            }
            cek = 0;
        } else {
            try {
                document.getElementById('backAgree').style.backgroundColor='white';
                document.getElementById('textAgree').style.color='#387ef5';
            } catch (e){
                console.log(e.message);
            }
            cek = 1;
        }
        counter++;
        if(counter === 10) {
            clearInterval(i);
        }
    }, 200);
}


var socket = io.connect( 'http://timesheet.mpm-rent.net:3003', {path: 'socket.io'} );
// var socket = io.connect( 'http://36.37.122.106:3003', {path: '/socket.io'} );
var idTsm = 0;

var deviceInfo = {};

angular.module('starter.controllers', [])
.run(function(ClockSrv, tmsService, $ionicPopup, $ionicLoading, $window, $ionicHistory){
    try {
        var datas = $window.localStorage['username_pic'];
        socket.on( 'notif_tsm', function( data ) {
            try {
                tmsService.getAllPendingNotif().success(function (response) {
                    cordova.plugins.notification.local.requestPermission(function (granted) {
                      cordova.plugins.notification.local.schedule(response);
                    });


                    cordova.plugins.notification.badge.requestPermission(function (granted) {
                        if (granted)
                            cordova.plugins.notification.badge.set(response.badge);
                    });
                }).finally(function () {
                    
                });
            } catch(ex) {
                console.log(ex);
            }
        });
    } catch (ex) {
        console.log(ex.message);
    }
})
.controller('AppCtrl', function($scope, tmsService, $location, $ionicPlatform, $timeout, $stateParams, ionicMaterialMotion, ionicMaterialInk, $ionicPopup, $ionicLoading, $window, $ionicHistory) {
    var page = 0;
    // Form data for the login modal
    $scope.loginData = {};
    $scope.isExpanded = false;
    $scope.hasHeaderFabLeft = false;
    $scope.hasHeaderFabRight = false;
    $scope.menu = true;
    $scope.version = "v2.1.3";

    // $cordovaBadge.set(3).then(function() {
    //     // You have permission, badge set.
    //   }, function(err) {
    //     // You do not have permission.
    // });

    $scope.show = function () {
        $ionicLoading.show({
            template: '<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>'
        });
    };

    $scope.showAllert = function (msg) {
        $ionicPopup.alert({
            title: '<i class="icon ion-alert"></i> '+ language['inf'],
            template: msg.message,
            okText: 'Ok',
            okType: 'button-positive',
            enableBackdropDismiss: false
        });
    };
    

    var back = 0;
    $ionicPlatform.registerBackButtonAction(function (e) {
        e.preventDefault();
        $ionicHistory.goBack();
        // $ionicGoBack($event);
        // $ionicPopup.confirm({
        //     template: 
        //     language['questexit'],
        //     title: '<i class="icon ion-information-circled"></i> '+language['inf'],
        //     scope: $scope,
        //     buttons: [
        //         {text: language['batal']},
        //         {
        //             text: language['ya'],
        //             type: 'button-positive',
        //             onTap: function (e) {
        //                 navigator.app.exitApp();
        //             }
        //         }
        //     ]
        // });
        return false;
      }, 100);

    $scope.hide = function () {
        $timeout(function() {
            $ionicLoading.hide();
        }, 100);
    };

    var buttonOut = function() {
        // $scope.fabMenu = {};
        // $scope.fabMenu.active = true;
        $timeout(function() {
            var options = {
                baseAngle: 90,
                rotationAngle: 50,
                distance: 90,
                animateInOut: 'slide', // can be slide, rotate, all
            };
            
            var buttonContainers = document.querySelectorAll('.fab-menu-items > li');
            for (var i = 0; i < buttonContainers.length; i++) {
                var button = buttonContainers.item(i);
                var angle = (options.baseAngle + (options.rotationAngle * i));
                button.setAttribute('angle', '' + angle);
                button.style.transform = "rotate(" + options.baseAngle + "deg) translate(0px) rotate(-" + options.baseAngle + "deg) scale(0)";
                button.style.WebkitTransform = "rotate(" + options.baseAngle + "deg) translate(0px) rotate(-" + options.baseAngle + "deg) scale(0)";
            }
        }, 100);
    }

    //Approve Timesheet
    $scope.datapin = {};
    $scope.getApproveById = function () {
        buttonOut();
        // var overlay = document.getElementsById('overlay');
        // overlay.removeClass("active");
        // <hr>あなたが受け取ったサービスに価値を与える
        $ionicPopup.prompt({
            template: 
            language['rateservice']+' <hr>'+'<div style="text-align: center;" class="radio-toolbar">'+
            '<input onClick="radioChecked(1)" type="radio" ng-model="datapin.radio" value="1" name="radio" id="radio1"/>'+
            '<label for="radio1" class="rad">'+
            '<img id="radioImg1" src="img/Bintang-Abu.png">'+
            '</label>'+
            '<input onClick="radioChecked(2)" type="radio" ng-model="datapin.radio" value="2" name="radio" id="radio2"/>'+
            '<label for="radio2" class="rad">'+
            '<img id="radioImg2" src="img/Bintang-Abu.png">'+
            '</label>'+
            '<input onClick="radioChecked(3)" type="radio" ng-model="datapin.radio" value="3" name="radio" id="radio3"/>'+
            '<label for="radio3" class="rad">'+
            '<img id="radioImg3" src="img/Bintang-Abu.png">'+
            '</label>'+
            '<input onClick="radioChecked(4)" type="radio" ng-model="datapin.radio" value="4" name="radio" id="radio4"/>'+
            '<label for="radio4" class="rad">'+
            '<img id="radioImg4" src="img/Bintang-Abu.png">'+
            '</label>'+
            '<input onClick="radioChecked(5)" type="radio" ng-model="datapin.radio" value="5" name="radio" id="radio5"/>'+
            '<label for="radio5" class="rad">'+
            '<img id="radioImg5" src="img/Bintang-Abu.png">'+
            '</label>'+
            '</div></ion-list>',
            title: '<i class="icon ion-information-circled"></i> '+language['setujuabs'],
            scope: $scope,
            buttons: [
                {text: language['batal']},
                {
                    text: language['ya'],
                    type: 'button-positive',
                    onTap: function (e) {
                        if ((!$scope.datapin.radio)) {
                            $scope.show('<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>');
                    
                            $scope.showAllert({
                                title: language['inf'],
                                message: language['rateservice']
                            });
                            // e.preventDefault();
                        } else {
                            var data = {
                                id: idTsm,
                                username: $window.localStorage['username_pic'],
                                survey: $scope.datapin.radio
                            };
                            tmsService.getApproveById(data).success(function (response) {
                                $scope.show('<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>');
                    
                                $window.location.href = "#/app/home";
                                $scope.showAllert({
                                    title: language['inf'],
                                    message: language[response.comment]
                                });
                            }, function (err) {
                                $scope.showAllert({
                                    title: '<i class="icon ion-minus-circled"></i> '+language['err'],
                                    message: err
                                });
                            }).finally(function () {
                                $scope.hide();
                            });
                        }
                    }
                }
            ]
        });
    };

    //Not Approve Timesheet
    $scope.getNotApproveById = function () {
        $scope.data = {desc:'Waktu absensi masuk tidak sesuai'};
        buttonOut();
        // var overlay = document.getElementsByClassName('fab-menu-overlay');
        // overlay[0].classList.remove("active");

        $scope.reasonList = [
          { value: 'reason1',text: language['reason1']},
          { value: 'reason2',text: language['reason2'] },
          { value: 'reason3',text: language['reason3'] },
          { value: 'reason4',text: language['reason4'] },
          { value: 'reason5',text: language['reason5'] },
          { value: 'reason6',text: language['reason6']}
          /*,
          { value: 'Lain-lain',text: language['reason7'] }*/
        ];

        $ionicPopup.prompt({
            /*template: '<ion-list class="list"><ion-lable style="padding: 10px 0px;white-space:normal;" class="item item-input">'+language['reasontabs']+'</ion-lable>'+
            '<textarea style="padding: 5px 10px;" rows="4" placeholder="'+language['descnotapp']+'" ng-model="data.desc" type="text"></textarea></ion-list>'*/
            /*template:'<div class="list">' +
              '<ion-radio ng-repeat="item in reasonList" ng-value="item.value" ng-model="data.desc" class="radio-positive">' +
              '{{ item.text }}' +
              '</ion-radio>' +
              '</div>',*/
            templateUrl: "templates/option-reason.html",
            title: '<i class="icon ion-information-circled"></i>'+language['tsetujuabs'],
            scope: $scope,
            buttons: [
                {text: language['batal']},
                {
                    text: language['ya'],
                    type: 'button-positive',
                    onTap: function () {
                        $scope.show('<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>');
                    
                        var data = {
                            id: idTsm,
                            username: $window.localStorage['username_pic'],
                            desc: $scope.data.desc
                        };
                        if (!$scope.data.desc) {
                            $scope.showAllert({
                                title: '<i class="icon ion-alert"></i> '+language['inf'],
                                message: language['reasontabs']
                            });
                        } else {
                          console.log($scope.data.desc);
                          tmsService.getNotApproveById(data).success(function (response) {
                              $scope.show('<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>');

                              $window.location.href = "#/app/home";
                              $scope.showAllert({
                                  title: language['inf'],
                                  message: language[response.comment]
                              });
                          }, function (err) {
                              $scope.showAllert({
                                  title: '<i class="icon ion-minus-circled"></i> '+language['err'],
                                  message: err
                              });
                          }).finally(function () {
                              $scope.hide();
                          });
                        }
                    }
                }
            ]
        });
    };

    var navIcons = document.getElementsByClassName('ion-navicon');
    for (var i = 0; i < navIcons.length; i++) {
        navIcons.addEventListener('click', function() {
            this.classList.toggle('active');
        });
    }

    ////////////////////////////////////////
    // Layout Methods
    ////////////////////////////////////////

    $scope.hideNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
        document.getElementsByTagName('ion-side-menu')[0].style.display = 'none';
    };

    $scope.showNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
        document.getElementsByTagName('ion-side-menu')[0].style.display = 'block';
    };

    $scope.noHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }
    };

    $scope.setExpanded = function(bool) {
        $scope.isExpanded = bool;
    };

    $scope.setHeaderFab = function(location) {
        var hasHeaderFabLeft = false;
        var hasHeaderFabRight = false;

        switch (location) {
            case 'left':
                hasHeaderFabLeft = true;
                break;
            case 'right':
                hasHeaderFabRight = true;
                break;
        }

        $scope.hasHeaderFabLeft = hasHeaderFabLeft;
        $scope.hasHeaderFabRight = hasHeaderFabRight;
    };

    $scope.hasHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (!content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }
    };

    $scope.hideHeader = function() {
        $scope.hideNavBar();
        $scope.noHeader();
    };

    $scope.showHeader = function() {
        $scope.showNavBar();
        $scope.hasHeader();
    };

    $scope.clearFabs = function() {
        var fabs = document.getElementsByName('fabContent');
        if (fabs.length && fabs.length > 0) {
            fabs[0].innerHTML = '';
        }
    };

    // document.addEventListener('deviceready', function () {
    // }, false);
    // cordova.plugins.notification.badge.set(8);

    $scope.doLogout = function () {
        // $ionicHistory.clearCache();
        $window.localStorage['username_pic'] = '';
        $window.localStorage['password_pic'] = '';
        $window.localStorage['id_lang'] = 1;
        $window.location.href = '#/app/login';
        $ionicHistory.clearHistory();
        $ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
        $window.localStorage['token'] = null;
        $window.localStorage['uid'] = "1654364x564sdfsdfrtR?!@323#Sd%";
        // $window.localStorage['username'] = 'aesdfawd';
        // alert($window.localStorage['username']);
        // window.location = 'http://timesheet.mpm-rent.net/';
        // window.location = server;
    };

    $scope.AboutPage = function () {
        // $scope.version = $window.localStorage['version'];
        // $scope.version = "v2.0.0";
        try {
            cordova.getAppVersion.getVersionNumber().then(function (version) {
                // var appVersion = version;
                $scope.getVersionCode = version;
            });
        } catch (e) {
            console.log(e.message);
        }
        $ionicPopup.alert({
            title: language['tentang'],
            templateUrl: 'templates/about.html',
            okText: 'Ok',
            okType: 'button-positive'
        });
    }
    $scope.InfoPage = function () {
        // $scope.version = $window.localStorage['version'];
        // $scope.version = "v2.0.0";
        $ionicPopup.alert({
            scope: $scope,
            title: language['info'],
            templateUrl: 'templates/info.html',
            okText: 'Ok',
            okType: 'button-positive'
        });
    }
    
    $scope.$on('$stateChangeSuccess', 
    function(event, toState, toParams, fromState, fromParams){ 
        var path = $location.path();
        var logo = document.getElementById('logoHead');
        if (page != 0) {
            if (path.indexOf('home') != -1) {
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                logo.style.width = '30px';
            } else {
                logo.style.width = '0px';
            }
        }
        page++;
        
        buttonOut();
        $scope.show();
    });

    $scope.datalang = {};
    var connect = false;
    if (language == null) {
        tmsService.getLanguage().success(function (response) {
            response.forEach(function(entry) {
                // alert(entry.indonesia);
                ind[entry.key] = entry.indonesia;
                eng[entry.key] = entry.english;
                jap[entry.key] = entry.japan;
                connect = true;
            });
        }, function (err) {
            $scope.showAllert({
                title: '<i class="icon ion-minus-circled"></i> Error',
                message: err
            });

        }).finally(function () {
            if ($window.localStorage['id_lang'] == 2) {
                $scope.language = eng;
                language = eng;
            } else if ($window.localStorage['id_lang'] == 3) {
                $scope.language = jap;
                language = jap;
            } else {
                $scope.language = ind;
                language = ind;
            }

            $timeout(function() {
                if( connect == false){
                    $ionicPopup.confirm({
                        template: language['tdkKonek'],
                        title: '<i class="icon ion-minus-circled"></i> Kesalahan Jaringan',
                        scope: $scope,
                        buttons: [
                            {
                                text: "Ok",
                                type: 'button-positive',
                                onTap: function (e) {
                                    navigator.app.exitApp();
                                }
                            }
                        ]
                    });
                }
            },1000);
        });
    }

    $scope.radioChecked = function (id) {
        $window.localStorage['id_lang'] = id;
        if ($window.localStorage['id_lang'] == 1) {
            $scope.language = ind;
            language = ind;
        } else if ($window.localStorage['id_lang'] == 2) {
            $scope.language = eng;
            language = eng;
        } else if ($window.localStorage['id_lang'] == 3) {
            $scope.language = jap;
            language = jap;
        }
    }
})

.controller('LoginCtrl', function($scope, tmsService, $timeout, $stateParams, ionicMaterialInk, $ionicPopup, $ionicLoading, $window, $ionicHistory) {
    $scope.$parent.clearFabs();
    $timeout(function() {
        $scope.$parent.hideHeader();

        $scope.hide();
    }, 100);

    ionicMaterialInk.displayEffect();
    $scope.menu = false;
    
    ionic.Platform.ready(function(){
        var device = ionic.Platform.device();
        // $scope.showAllert({
        //     title: '<i class="icon ion-alert"></i> Informasi Pembaharuan Aplikasi !',
        //     message: device.version
        // });
        $window.localStorage['uuid'] = device.uuid;
        $window.localStorage['model'] = device.model;
        $window.localStorage['manufacturer'] = device.manufacturer;
        $window.localStorage['version'] = device.version;
        $window.localStorage['platform'] = device.platform;
        deviceInfo = {
            uuid : device.uuid,
            model : device.model,
            manufacturer : device.manufacturer,
            version : device.version,
            platform : device.platform
        }
        try {
            // cordova.plugins.notification.badge.configure({ autoClear: true });
            // cordova.plugins.notification.badge.set(8);
            // var data = {
            //     badge: 5,
            //     wasTapped: true                                               
            // };
            // FCMPlugin.onNotification(function(data){
            //     cordova.plugins.notification.badge.set(data.badge);
            //     cordova.plugins.notification.badge.configure({ autoClear: true });
            //     if(data.wasTapped == true){
            //         alert( JSON.stringify(data) );
            //         cordova.plugins.notification.badge.configure({ autoClear: true });
            //     } else {
            //         alert( JSON.stringify(data) );
            //         cordova.plugins.notification.badge.clear();
            //     }
            // });

            cordova.getAppVersion.getVersionNumber().then(function (version) {
                // var appVersion = version;
                $scope.getVersionCode = version;
                $window.localStorage['version_app'] = version;
                tmsService.checkUpdate($window.localStorage['version_app']).success(function (response) {
                    console.log(response);
                    if (response.row == 1) {
                        $scope.showAllert({
                            title: '<i class="icon ion-alert"></i> '+language['infupdate'],
                            message: response.update_desc.replace(/(?:\r\n|\r|\n)/g, '<br />').replace('<br /><br />', '<hr><br />'),
                            enableBackdropDismiss: false
                        });
                        $window.localStorage['req_update'] = 1;
                    }
                });
            });
        } catch (e) {
            console.log(e.message);
        }
    });
    // console.log($window.localStorage);

    // if (language == null) {
    //     // language['inf'] = '';
    //     // language['fillpass'] = '';
    //     var language = {
    //         inf: '',
    //         fillpass: ''
    //     };
    // }

    $scope.showAllert = function (msg) {
        $ionicPopup.alert({
            title: '<i class="icon ion-alert"></i> '+ language['inf'],
            template: msg.message,
            okText: 'Ok',
            okType: 'button-positive',
            enableBackdropDismiss: false
        });
    };
    $scope.data = {};
    $scope.data.uname = $window.localStorage['username_pic'];
    $scope.data.passwd = $window.localStorage['password_pic'];
    
    $scope.login = function () {
        if ($window.localStorage['req_update'] == 1) {
            $scope.showAllert({
                title: '<i class="icon ion-alert"></i> '+ language['inf'],
                message: language['appupdate']
            });
        } else 
        if (!$scope.data.uname) {
            $scope.showAllert({
                title: '<i class="icon ion-alert"></i> '+language['inf'],
                message: language['filluser']
            });
        } else if (!$scope.data.passwd) {
            $scope.showAllert({
                title: '<i class="icon ion-alert"></i> '+language['inf'],
                message: language['fillpass'] //language['fillpass']
            });
        } else {
            var data = {
                uname: $scope.data.uname,
                passwd: $scope.data.passwd
            };
            $scope.show = function () {
                $ionicLoading.show({
                    template: '<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>'
                });
            };
            $scope.hide = function () {
                $timeout(function() {
                    $ionicLoading.hide();
                }, 100);
            };
            $scope.show();
            tmsService.doLogin(data).success(function (resp) {
                if (resp.status == 1) {
                    
                    $ionicHistory.clearHistory();
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                    $window.localStorage['password_pic'] = $scope.data.passwd;
                    $scope.data.uname = "";
                    $scope.data.passwd = "";
                    $scope.uid = resp.uid;
                    $scope.token = resp.token;
                    user_pic = resp.username;
                    $window.localStorage['username_pic'] = resp.username;
                    $window.localStorage['name'] = resp.name;
                    $window.localStorage['uid'] = $scope.uid;
                    $window.localStorage['user_type'] = resp.user_type;
                    $window.localStorage['token'] = $scope.token;
                    $window.location.href = '#/app/home';
                    // tmsService.doCheckTimesheet().success(function (resp) {
                    //     if (resp.length > 0) {
                    //         $scope.showAllert({
                    //             title: '<i class="icon ion-minus-circled"></i> WARNING',
                    //             message: 'Kamu mempunyai ' + resp.length + ' Timesheet yang belum di approve.'
                    //         });
                    //     }
                    // });
                } else if (resp.status == 3) {
                    $scope.showAllert({
                        title: '<i class="icon ion-minus-circled"></i> '+ language['inf'],
                        message: language['infgs'] 
                    });
                } else {
                    $scope.showAllert({
                        title: '<i class="icon ion-minus-circled"></i> '+ language['inf'] ,
                        message: language['inffailog']
                    });
                }
            }, function (err) {
                $scope.showAllert({
                    title: '<i class="icon ion-minus-circled"></i> '+language['err'],
                    message: err
                });
            }).finally(function () {
                $scope.hide();
            });
        }
    };
    if (($window.localStorage['username_pic'] != null) && ($window.localStorage['username_pic'] != '') ) {
        $scope.radioChecked($window.localStorage['id_lang']);
        console.log($window.localStorage['id_lang']);
        $scope.login();
    }
})

.controller('HomeCtrl', function($scope, $ionicPlatform, tmsService, $timeout, $stateParams, ionicMaterialMotion, ionicMaterialInk, $ionicPopup, $ionicLoading, $window, $ionicHistory) {
    $scope.uid = $window.localStorage['uid'];
    $scope.show = function () {
        $ionicLoading.show({
            template: '<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>'
        });
    };


    $scope.check = {};
    var data = {
        username: $window.localStorage['username_pic']                                                   
    };
    tmsService.getNotifAgree(data).success(function (response) {
        if (response.agree == 0) {
            $ionicPlatform.registerBackButtonAction(function(){
                // do nothing
            }, 401);
            $ionicPopup.prompt({
                template: '<ion-list class="list"> <ul style="text-align: justify; !important">'+
                language['notifAgree'] +
                
                '<ion-checkbox id="backAgree" style="padding: 10px; padding-left: 60px;white-space: inherit;font-size: 13px;" ng-model="check.checkAgr"> <span id="textAgree" style="color: #387ef5; font-weight: 600;white-space: normal;text-align: justify;">'+language['cekNotifAgree']+'</span></ion-checkbox><br>'+
                '</ul></ion-list>',
                title: '<i class="icon ion-alert"></i> '+language['inf'],
                scope: $scope,
                buttons: [
                    {
                        text: 'Ya',
                        type: 'button-positive',
                        onTap: function (e) {
                            if (!$scope.check.checkAgr) {
                                e.preventDefault();
                                alertButton();
                            } else {
                                var data = {
                                    username: $window.localStorage['username_pic'],
                                    device : deviceInfo                                                      
                                };
                                tmsService.getAgree(data).success(function (resp) {
                                    $scope.res = resp;
                                }, function (err) {
                                    $ionicPopup.alert({
                                        title: '<i class="icon ion-minus-circled"></i> ERROR',
                                        template: err
                                    });
                                }).finally(function () {
                                    $scope.hide();
                                });
                                $ionicPlatform.registerBackButtonAction(function(e){
                                    e.preventDefault();
                                    $ionicHistory.goBack();
                                    return false;
                                }, 100);
                            }
                        }
                    }
                ]
            });
        }
    }).finally(function () {
    });

    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    // Set Motion
    $timeout(function() {
        ionicMaterialMotion.slideUp({
            selector: '.slide-up'
        });

        $timeout(function() {
            ionicMaterialMotion.fadeSlideInRight({
                startVelocity: 7000
            });
        }, 100);
    }, 100);

    // Set Ink
    ionicMaterialInk.displayEffect();

    $scope.hide = function () {
        $timeout(function() {
            $ionicLoading.hide();
        }, 100);
    };
    $scope.show();
    $scope.tmss = [];
    tmsService.getAllPending().success(function (response) {
        $scope.tmss = response;
        console.log(response);
        $scope.count = response.length;

        // document.addEventListener('deviceready', function () {
        try {
            cordova.plugins.notification.badge.requestPermission(function (granted) {
                if (granted)
                    cordova.plugins.notification.badge.set(response.length);
                cordova.plugins.notification.badge.get(function (badge) {
                    console.log(badge);
                });
            });
        } catch (e) {
            console.log(e.message);
        }
        // }, false);
    }).finally(function () {
        $scope.hide();
        // Set Motion
        $timeout(function() {
            ionicMaterialMotion.slideUp({
                selector: '.slide-up'
            });
    
            $timeout(function() {
                ionicMaterialMotion.fadeSlideInRight({
                    startVelocity: 7000
                });
            }, 100);
        }, 100);
        ionicMaterialInk.displayEffect();
    });
    $scope.username = $window.localStorage['name'];
    $scope.access = $window.localStorage['user_type'];
    $scope.doRefresh = function () {
        tmsService.getAllPending().success(function (response) {
            $scope.tmss = response;
            $scope.count = response.length;
        }).finally(function () {
            $scope.$broadcast('scroll.refreshComplete');

            // Set Motion
            $timeout(function() {
                ionicMaterialMotion.slideUp({
                    selector: '.slide-up'
                });
            }, 100);
    
            $timeout(function() {
                ionicMaterialMotion.fadeSlideInRight({
                    startVelocity: 7000
                });
            }, 100);
            // Set Ink
            ionicMaterialInk.displayEffect();
        });
    };
    $scope.viewDetails = function (id) {
        $window.location.href = "#/app/pending_detail/" + id + "/yes";
    };
})
.controller('ApprovedCtrl', function($scope, tmsService, $timeout, $stateParams, ionicMaterialMotion, ionicMaterialInk, $ionicPopup, $ionicLoading, $window, $ionicHistory) {
    $scope.uid = $window.localStorage['uid'];
    $scope.show = function () {
        $ionicLoading.show({
            template: '<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>'
        });
    };
    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);
    $scope.show();

    $scope.hide = function () {
        $timeout(function() {
            $ionicLoading.hide();
        }, 100);
    };
    $scope.tmss = [];
    tmsService.getAllApprove().success(function (response) {
        $scope.tmss = response;
    }).finally(function () {
        // Set Motion
        $timeout(function() {
            ionicMaterialMotion.slideUp({
                selector: '.slide-up'
            });
        }, 100);
    
        $timeout(function() {
            ionicMaterialMotion.fadeSlideInRight({
                startVelocity: 7000
            });
            $scope.hide();
        }, 100);
        // Set Ink
        ionicMaterialInk.displayEffect();
        // $scope.hide();
    });
    $scope.doRefresh = function () {
        $scope.show();
        tmsService.getAllApprove().success(function (response) {
            $scope.tmss = response;
        }).finally(function () {
            $scope.$broadcast('scroll.refreshComplete');
            // Set Motion
            $timeout(function() {
                ionicMaterialMotion.slideUp({
                    selector: '.slide-up'
                });
            }, 100);
        
            $timeout(function() {
                ionicMaterialMotion.fadeSlideInRight({
                    startVelocity: 7000
                });
                $scope.hide();
            }, 100);
        
            // Set Ink
            ionicMaterialInk.displayEffect();
        });
    };
    $scope.viewDetails = function (id) {
        $window.location.href = "#/app/pending_detail/" + id + "/yes";
    };
})
.controller('DriversCtrl', function($scope, tmsService, $timeout, $stateParams, ionicMaterialMotion, ionicMaterialInk, $ionicPopup, $ionicLoading, $window, $ionicHistory) {
    $scope.uid = $window.localStorage['uid'];
    $scope.show = function () {
        $ionicLoading.show({
            template: '<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>'
        });
    };
    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    $scope.hide = function () {
        $timeout(function() {
            $ionicLoading.hide();
        }, 100);
    };
    $scope.show();
    $scope.tmss = [];
    var data = {
        username : $window.localStorage['username_pic']
    };
    tmsService.getAllDrivers(data).success(function (response) {
        $scope.tmss = response;
    }).finally(function () {

        // Set Motion
        $timeout(function() {
            ionicMaterialMotion.slideUp({
                selector: '.slide-up'
            });
        }, 100);
    
        $timeout(function() {
            ionicMaterialMotion.fadeSlideInRight({
                startVelocity: 7000
            });
        }, 100);
        ionicMaterialInk.displayEffect();
        $scope.hide();
    });
    $scope.doRefresh = function () {
        tmsService.getAllDrivers(data).success(function (response) {
            $scope.tmss = response;
        }).finally(function () {
            $scope.$broadcast('scroll.refreshComplete');
            // Set Motion
            $timeout(function() {
                ionicMaterialMotion.slideUp({
                    selector: '.slide-up'
                });
            }, 100);
        
            $timeout(function() {
                ionicMaterialMotion.fadeSlideInRight({
                    startVelocity: 7000
                });
            }, 100);
        
            // Set Ink
            ionicMaterialInk.displayEffect();
        });
    };
})
.controller('InboxCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {
    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 100);

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
})
.controller('ContactCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {
    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 100);

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
})
.controller('PasswordCtrl', function($scope, tmsService, $timeout, $stateParams, ionicMaterialInk, ionicMaterialMotion, $ionicPopup, $ionicLoading, $window, $ionicHistory) {
    $scope.change = {};
    $scope.show = function () {
    $ionicLoading.show({
            template: '<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>'
        });
    };
    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    $scope.showAllert = function (msg) {
        $ionicPopup.alert({
            title: '<i class="icon ion-alert"></i> '+ language['inf'],
            template: msg.message,
            okText: 'Ok',
            okType: 'button-positive',
            enableBackdropDismiss: false
        });
    };

    // $ionicHistory.nextViewOptions({
    //     disableBack: false
    // });

    $scope.hide = function () {
        $timeout(function() {
            $ionicLoading.hide();
        }, 100);
    };

    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
        $scope.hide();
    }, 100);

    $scope.changePassword = function () {
        if (!$scope.change.old_password) {
            $scope.showAllert({
                title: '<i class="icon ion-alert"></i> '+language['inf'],
                message: language['infpassold']
            });
        } else if (!$scope.change.new_password) {
            $scope.showAllert({
                title: '<i class="icon ion-alert"></i> '+language['inf'],
                message: language['infpassnew']
            });
        } else if (!$scope.change.konf_new_password) {
            $scope.showAllert({
                title: '<i class="icon ion-alert"></i> '+language['inf'],
                message: language['infpassnewcon']
            });
        } else if ($scope.change.new_password != $scope.change.konf_new_password) {
            $scope.showAllert({
                title: '<i class="icon ion-alert"></i> '+language['inf'],
                message: language['inferrpass']
            });
        } else {
            $scope.show('<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>');
            var data = {
                username: $window.localStorage['username_pic'],
                old_password: $scope.change.old_password,
                new_password: $scope.change.new_password,
                konf_new_password: $scope.change.konf_new_password
            };
            tmsService.getChangePassword(data).success(function (response) {

                $scope.showAllert({
                    title: language['inf'],
                    message: language[response.comment]
                });
            }, function (err) {
                $scope.showAllert({
                    title: '<i class="icon ion-minus-circled"></i> '+language['err'],
                    message: err
                });
            }).finally(function () {
                $scope.hide();
            });
            $window.location.href = '#/app/home';
        }
    };

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
})
.controller('AboutCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {
    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 100);

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
})
.controller('DetailCtrl', function($scope, tmsService, $timeout, $stateParams, ionicMaterialMotion, ionicMaterialInk, $ionicPopup, $ionicLoading, $window, $ionicHistory) {
    $scope.show = function () {
        $ionicLoading.show({
            template: '<strong class="balanced-900 bold balanced-100-bg"><div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></strong>'
        });
    };

    idTsm = $stateParams.id;
    // Set Header
    $scope.$parent.showHeader();
    $scope.isExpanded = true;
    $scope.$parent.setExpanded(true);
    $scope.$parent.setHeaderFab('right');

    $scope.showAllert = function (msg) {
        $ionicPopup.alert({
            title: '<i class="icon ion-alert"></i> '+ language['inf'],
            template: msg.message,
            okText: 'Ok',
            okType: 'button-positive'
        });
    };

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
    if ($stateParams.type != 'pending') {
        $scope.info = 'false';
        $scope.title = 'setuju';
        $scope.$parent.clearFabs();
    } else {
        $scope.info = 'true';
        $scope.title = 'tsetuju';
    }

    $scope.hide = function () {
        $timeout(function() {
            $ionicLoading.hide();
        }, 100);
    };
    $scope.show();
    $scope.tmss = [];
    tmsService.getTimesheetById($stateParams.id).success(function (response) {
        // $scope.tmss = response;
        //console.log(response);
        angular.forEach(response, function (value, key) {
          if(key==='Not Approve Desc' && value!=''){
            value = language[value]
          }
          $scope.tmss[key] = value;
        });
    }).finally(function () {
        $scope.hide();
    
        $timeout(function() {
            try {
                ionicMaterialMotion.fadeSlideIn({
                    selector: '.animate-fade-slide-in .item'
                });
            } catch (e) {
    
            }
        }, 100);
        ionicMaterialInk.displayEffect();
    });

    $scope.notSorted = function(obj){
        if (!obj) {
            return [];
        }
        return Object.keys(obj);
    }
}).run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })
  .directive('fabMenu', function($timeout, $ionicGesture) {

    var options = {
        baseAngle: 90,
        rotationAngle: 50,
        distance: 90,
        animateInOut: 'slide', // can be slide, rotate, all
      },
      buttons = [],
      buttonContainers = [],
      buttonsContainer = null,
      lastDragTime = 0,
      currentX = 0,
      currentY = 0,
      previousSpeed     = 15,

      init = function() {

        buttons = document.getElementsByClassName('fab-menu-button-item');
        buttonContainers = document.querySelectorAll('.fab-menu-items > li');
        buttonsContainer = document.getElementsByClassName('fab-menu-items');

        for (var i = 0; i < buttonContainers.length; i++) {

          var button = buttonContainers.item(i);
          var angle = (options.baseAngle + (options.rotationAngle * i));
          button.style.transform = "rotate(" + options.baseAngle + "deg) translate(0px) rotate(-" + options.baseAngle + "deg) scale(0)";
          button.style.WebkitTransform = "rotate(" + options.baseAngle + "deg) translate(0px) rotate(-" + options.baseAngle + "deg) scale(0)";
          button.setAttribute('angle', '' + angle);
        }
      },

      animateButtonsIn = function() {
        for (var i = 0; i < buttonContainers.length; i++) {

          var button = buttonContainers.item(i);
          var angle = button.getAttribute('angle');
          button.style.transform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(0.8)";
          button.style.WebkitTransform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(0.8)";
        }
      },
      animateButtonsOut = function() {
        for (var i = 0; i < buttonContainers.length; i++) {

          var button = buttonContainers.item(i);
          var angle = (options.baseAngle + (options.rotationAngle * i));
          button.setAttribute('angle', '' + angle);
          button.style.transform = "rotate(" + options.baseAngle + "deg) translate(0px) rotate(-" + options.baseAngle + "deg) scale(0)";
          button.style.WebkitTransform = "rotate(" + options.baseAngle + "deg) translate(0px) rotate(-" + options.baseAngle + "deg) scale(0)";
        }
      },

      rotateButtons = function(direction, speed) {

        // still looking for a better solution to handle the rotation speed
        // the direction will be used to define the angle calculation

        // max speed value is 25 // can change this :)
        // used previousSpeed to reduce the speed diff on each tick
        speed = (speed > 15) ? 15 : speed;
        speed = (speed + previousSpeed) / 2;
        previousSpeed = speed; 
        
        var moveAngle = (direction * speed);

        // if first item is on top right or last item on bottom left, move no more
        if ((parseInt(buttonContainers.item(0).getAttribute('angle')) + moveAngle >= 285 && direction > 0) ||
          (parseInt(buttonContainers.item(buttonContainers.length - 1).getAttribute('angle')) + moveAngle <= 345 && direction < 0)
        ) {
          return;
        }

        for (var i = 0; i < buttonContainers.length; i++) {

          var button = buttonContainers.item(i),
            angle = parseInt(button.getAttribute('angle'));

          angle = angle + moveAngle;

          button.setAttribute('angle', '' + angle);

          button.style.transform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(1)";
          button.style.WebkitTransform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(1)";
        }
      },

      endRotateButtons = function() {

        for (var i = 0; i < buttonContainers.length; i++) {

          var button = buttonContainers.item(i),
            angle = parseInt(button.getAttribute('angle')),
            diff = angle % options.rotationAngle;
          // Round the angle to realign the elements after rotation ends
          angle = diff > options.rotationAngle / 2 ? angle + options.rotationAngle - diff : angle - diff;

          button.setAttribute('angle', '' + angle);

          button.style.transform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(1)";
          button.style.WebkitTransform = "rotate(" + angle + "deg) translate(" + options.distance + "px) rotate(-" + angle + "deg) scale(1)";
        }
      };

    return {
      templateUrl: "templates/fab-menu.html",
      link: function(scope) {
        // console.info("fab-menu :: link");

        init();

        scope.fabMenu = {
          active: false
        };

        var menuItems = angular.element(buttonsContainer);

        $ionicGesture.on('touch', function(event) {

        //   console.log('drag starts', event);
          lastDragTime = 0;
          currentX = event.gesture.deltaX;
          currentY = event.gesture.deltaY;
          previousSpeed = 0;

        }, menuItems)

        $ionicGesture.on('release', function(event) {
        //   console.log('drag ends');
          endRotateButtons();
        }, menuItems);

        $ionicGesture.on('drag', function(event) {

          if (event.gesture.timeStamp - lastDragTime > 100) {
            var direction = 1,
              deltaX = event.gesture.deltaX - currentX,
              deltaY = event.gesture.deltaY - currentY,
              delta = Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));

            if ((deltaX <= 0 && deltaY <= 0) || (deltaX <= 0 && Math.abs(deltaX) > Math.abs(deltaY))) {
              direction = -1;
            } else if ((deltaX >= 0 && deltaY >= 0) || (deltaY <= 0 && Math.abs(deltaX) < Math.abs(deltaY))) {
              direction = 1;
            }

            rotateButtons(direction, delta);

            lastDragTime = event.gesture.timeStamp;
            currentX = event.gesture.deltaX;
            currentY = event.gesture.deltaY;
          }
        }, menuItems);

        scope.fabMenuToggle = function() {

          if (scope.fabMenu.active) { // Close Menu
            animateButtonsOut();
          } else { // Open Menu
            animateButtonsIn();
          }
          scope.fabMenu.active = !scope.fabMenu.active;
        }

      }
    }
  })
